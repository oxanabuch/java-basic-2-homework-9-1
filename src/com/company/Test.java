package com.company;

public class Test {
    public static void main(String[] args) {

        System.out.println("Сума чисел (int): " + Calc.add(125, 17));
        System.out.println("Сума чисел (double): " + Calc.add(103.5, 15.8));

        System.out.println("Різниця чисел (int): " + Calc.sub(125, 17));
        System.out.println("Різниця чисел (double): " + Calc.sub(103.5, 15.8));

        System.out.println("Добуток чисел (int): " + Calc.mul(125, 17));
        System.out.println("Добуток чисел (double): " + Calc.mul(103.5, 15.8));

        System.out.println("Частка чисел (int): " + Calc.div(125, 17));
        System.out.println("Частка чисел (double): " + Calc.div(103.5, 15.8));
    }
}
